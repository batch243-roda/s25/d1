console.log("Hello JSON!");

// [Section] JSON Objects
/**
 * - JSON stands for JavaScript Object Notation
 * - JSON is also used in other programming languages hence the name JSON
 * - Core JavaScript has a built in JSON Object that contains methods for parsing JSON objects and converting strings into JavaScripts Objects.
 * - JSON is used for serializing different data types.
 * Syntax:
 *    {
 *      "propertA": "valueA",
 *      "propertyB": "valueB"
 *    }
 */
// // JSON Object
// {
//   "city": "Quezon City",
//   "province": "Metro Manila",
//   "country": "Philippines"
// }
// // JSON Arrays:
// [
//   {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"
//   },
//   {
//     "city": "Manila City",
//     "province": "Metro Manila",
//     "country": "Philippines"
//   }
// ]

// [Section] JSON methods
/**
 * - The JSON object contains method for parsing and converting data into stringified JSON
 */
const batchesArr = [
  {
    batchName: "Batch X",
  },
  {
    batchName: "Batch Y",
  },
];
// The stringify method is used to convert JavaScript objects into a string
console.log(batchesArr);
console.log("Result from stringify method:");
const stringBatchesArr = JSON.stringify(batchesArr);
console.log(stringBatchesArr, typeof stringBatchesArr);

const data = JSON.stringify({
  name: "John",
  address: {
    city: "Manila",
    country: "Philippines",
  },
});
console.log(data);

// [Section] Using stringify method with variables
// When information is stored in variable and is not hard coded into an object that is being stringified, we can supply the value with a variable

// User details
// const firstName = prompt("What is your first name?");
// const lastName = prompt("What is your last name?");
// const age = prompt("How young are you");
// const address = {
//   city: prompt("Which city do you live in?"),
//   country: prompt("Which country does your city address belong to?"),
// };

// let otherData = JSON.stringify({
//   firstName,
//   lastName,
//   age,
//   address,
// });

// console.log(otherData);

// [Section] Converting stringified JSON into JavaScript Objects
/**
 * - Objects are common data types used in appliction because of the complex data structures that can be created out of them
 * - Information is commonly sent to applications in stringified JSON and then converted back into objects
 * - This happens both for sending information to a backend application and sending information bact to a frontend application.
 */
const batchesJSON = `[
    {"batchNameA": "Batch X"},
    {"bacthNameB": "Batch Y"}
]`;
const parsedBatchesJSON = JSON.parse(batchesJSON);
console.log(parsedBatchesJSON);
console.log(parsedBatchesJSON[0]);

const parsedObject = `{
  "name": "John",
  "age": "31",
  "address": {
    "city": "Manila",
    "country": "Philippines"
  }
}`;
console.log(JSON.parse(parsedObject));
